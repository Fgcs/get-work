import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserModel } from 'src/app/auth/models/user.model';
import { CreatePostModel } from '../../models/createPost.model';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-manager-create-job',
  templateUrl: './manager-create-job.component.html',
  styleUrls: ['./manager-create-job.component.css'],
})
export class ManagerCreateJobComponent implements OnInit {
  form: FormGroup = this.fb.group({
    title: ['', Validators.required],
    description: ['', Validators.required],
    position: ['', Validators.required],
    company: ['', Validators.required],
    typeOfWorkplace: [null, Validators.required], //
    teme: ['', Validators.required],
    typeJob: [null, Validators.required], //
    address: ['', Validators.required],
  });
  loading: boolean = false;
  user: UserModel = JSON.parse(localStorage.getItem('user') as string);
  constructor(
    private fb: FormBuilder,
    private managerService: ManagerService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  save() {
    if (this.form.invalid) return;
    this.loading = true;
    const body: CreatePostModel = {
      title: this.form.value.title,
      description: this.form.value.description,
      position: this.form.value.position,
      company: this.form.value.company,
      typeOfWorkplace: this.form.value.typeOfWorkplace,
      teme: this.form.value.teme,
      typeJob: this.form.value.typeJob,
      address: this.form.value.address,
      userId: this.user._id as string,
    };
    this.managerService.create(body).subscribe((resp) => {
      this.loading = false;
      this.router.navigateByUrl('/manager/manager-job');
    });
  }
}
