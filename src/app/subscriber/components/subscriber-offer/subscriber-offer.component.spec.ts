import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubscriberOfferComponent } from './subscriber-offer.component';

describe('SubscriberOfferComponent', () => {
  let component: SubscriberOfferComponent;
  let fixture: ComponentFixture<SubscriberOfferComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubscriberOfferComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubscriberOfferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
