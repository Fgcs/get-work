import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { managerRoutingModule } from './manager-routing.module';

import { ManagerHomeComponent } from './components/manager-home/manager-home.component';
import { ManagerOfferComponent } from './components/manager-offer/manager-offer.component';
import { ManagerJobComponent } from './components/manager-job/manager-job.component';
import { ManagerCreateJobComponent } from './components/manager-create-job/manager-create-job.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ ManagerHomeComponent, ManagerOfferComponent, ManagerJobComponent, ManagerCreateJobComponent,ManagerCreateJobComponent],
  imports: [CommonModule, managerRoutingModule, RouterModule,ReactiveFormsModule,FormsModule],
  exports: [ManagerHomeComponent]
})
export class ManagerModule { }
