import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';



import { SignInComponent } from './components/sign-in/sign-in.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SignUpManagerComponent } from './components/sign-up-manager/sign-up-manager.component';
import { SignUpSubscriberComponent } from './components/sign-up-subscriber/sign-up-subscriber.component';


const routes: Routes = [
  {
    path: 'login',
    component: SignInComponent,
  },
  {
    path: 'register',
    component: SignUpComponent,
  },
  {
    path: 'register-subscriber',
    component: SignUpSubscriberComponent,
  },
  {
    path: 'register-manager',
    component: SignUpManagerComponent,
  },
  {
    path: '**',
    redirectTo: 'login',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
