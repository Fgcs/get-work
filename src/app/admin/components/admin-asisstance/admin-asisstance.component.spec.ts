import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAsisstanceComponent } from './admin-asisstance.component';

describe('AdminAsisstanceComponent', () => {
  let component: AdminAsisstanceComponent;
  let fixture: ComponentFixture<AdminAsisstanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAsisstanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAsisstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
