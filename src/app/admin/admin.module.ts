import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { adminRoutingModule } from './admin-routing.module';

import { AdminAsisstanceComponent } from './components/admin-asisstance/admin-asisstance.component';
import { AdminContentsComponent } from './components/admin-contents/admin-contents.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { AdminJobComponent } from './components/admin-job/admin-job.component';
import { AdminThemeComponent } from './components/admin-theme/admin-theme.component';
import { AdminNavbarComponent } from './components/admin-navbar/admin-navbar.component';
import { AdminTopicComponent } from './components/admin-topic/admin-topic.component';

@NgModule({
  declarations: [ 
    AdminAsisstanceComponent, 
    AdminContentsComponent, 
    AdminHomeComponent,
    AdminJobComponent, 
    AdminThemeComponent,
    AdminTopicComponent, 
    AdminNavbarComponent,
    AdminNavbarComponent
  ],
  imports: [CommonModule, adminRoutingModule, RouterModule],
  exports: [AdminHomeComponent]
})
export class AdminModule { }
