import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthModel } from '../../models/auth.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {
  loading: boolean = false;
  form: FormGroup = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  onSubmit() {
    if (this.form.invalid) return;
    this.loading = true;
    this.authService.login(this.form.value).subscribe(
      (resp: AuthModel) => {
        this.loading = false;
        const { user } = resp;
        localStorage.setItem('token', resp.token as string);
        localStorage.setItem('user', JSON.stringify(user));
        if (user?.rol === 'User') {
          if (user.accountType === 'Professional') {
            this.router.navigateByUrl('/subscriber/subscriber-home');
          } else {
            this.router.navigateByUrl('/manager/manager-home');
          }
        } else {
          this.router.navigateByUrl('/admin/admin-home');
        }
      },
      (err) => {
        this.loading = false;
      }
    );
  }
}
