import { UserModel } from 'src/app/auth/models/user.model';

export class PostModel {
  _id?: string;
  title?: string;
  description?: string;
  position?: string;
  company?: string;
  typeOfWorkplace?: string; //
  teme?: string;
  typeJob?: string; //
  address?: string;
  status?: string;
  author?: UserModel;
  createdAt?: Date;
  updatedAt?: Date;
}
