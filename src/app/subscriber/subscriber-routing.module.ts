import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubscriberHomeComponent } from  './components/subscriber-home/subscriber-home.component';
import { SubscriberOfferComponent } from './components/subscriber-offer/subscriber-offer.component';

const routes: Routes = [
  {
    path: 'subscriber-home',
    component: SubscriberHomeComponent,
  },
  {
    path: 'subscriber-offer',
    component: SubscriberOfferComponent,
  },
  {
    path: '**',
    redirectTo: 'subscriber-home',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class subscriberRoutingModule {}
