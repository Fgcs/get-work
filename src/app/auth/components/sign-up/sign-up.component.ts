import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
})
export class SignUpComponent implements OnInit {
  loading: boolean = false;
  form: FormGroup = this.fb.group({
    name: ['', Validators.required],
    lastname: ['', Validators.required],
    dni: ['', Validators.required],
    email: ['', Validators.required],
    password: ['', Validators.required],
    accountType: [null, Validators.required],
    address: ['', Validators.required],
    webSite: ['', Validators.required],
  });
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  onSubmit() {
    if (this.form.invalid) return;
    this.loading = true;
    this.authService.register(this.form.value).subscribe((resp) => {
      if (resp.accountType === 'Professional') {
        this.router.navigateByUrl('/auth/login');
      } else {
        this.router.navigateByUrl('/auth/login');
      }
      this.loading = false;
    });
  }
}
