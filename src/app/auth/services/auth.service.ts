import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthModel } from '../models/auth.model';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(login: any): Observable<AuthModel> {
    return this.http.post<AuthModel>(`http://localhost:3000/auth/login`, login);
  }
  register(register: any): Observable<UserModel> {
    return this.http.post<AuthModel>(
      `http://localhost:3000/auth/register`,
      register
    );
  }
}
