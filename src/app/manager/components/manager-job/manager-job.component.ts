import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from 'src/app/auth/models/user.model';
import { ManagerService } from '../../services/manager.service';

@Component({
  selector: 'app-manager-job',
  templateUrl: './manager-job.component.html',
  styleUrls: ['./manager-job.component.css'],
})
export class ManagerJobComponent implements OnInit {
  user: UserModel = JSON.parse(localStorage.getItem('user') as string);
  posts: any;
  constructor(private managerService: ManagerService) {}

  ngOnInit(): void {
    this.getPost();
  }
  getPost() {
    this.managerService
      .getByAuthor(this.user._id as string)
      .subscribe((resp) => {
        this.posts = resp;
      });
  }
  onDelete(id: string) {
    console.log(id);

    this.managerService.delete(id).subscribe((resp) => {
      this.getPost();
    });
  }
}
