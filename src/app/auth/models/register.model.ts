export class RegisterModel {
  name!: string;
  lastname!: string;
  dni!: string;
  email!: string;
  password?: string;
  accountType!: string;
  address?: string;
  webSite?: string;
}
