import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PostModel } from 'src/app/manager/models/post.model';

@Injectable({
  providedIn: 'root',
})
export class SubscriberService {
  constructor(private http: HttpClient) {}

  getOffers(): Observable<PostModel[]> {
    return this.http.get<PostModel[]>(`http://localhost:3000/posts`);
  }
}
