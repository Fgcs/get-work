import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { subscriberRoutingModule } from './subscriber-routing.module';
import { SubscriberHomeComponent } from './components/subscriber-home/subscriber-home.component';
import { SubscriberOfferComponent } from './components/subscriber-offer/subscriber-offer.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ SubscriberHomeComponent, SubscriberOfferComponent],
  imports: [CommonModule, subscriberRoutingModule, RouterModule],
  exports: [SubscriberHomeComponent]
})
export class SubscriberModule { }
