import { Component, OnInit } from '@angular/core';
import { PostModel } from 'src/app/manager/models/post.model';
import { SubscriberService } from '../../services/subscriber.service';

@Component({
  selector: 'app-subscriber-offer',
  templateUrl: './subscriber-offer.component.html',
  styleUrls: ['./subscriber-offer.component.css'],
})
export class SubscriberOfferComponent implements OnInit {
  posts: PostModel[] = [];
  constructor(private subscriberService: SubscriberService) {}

  ngOnInit(): void {
    this.getOffers();
  }
  getOffers() {
    this.subscriberService.getOffers().subscribe((resp: PostModel[]) => {
      this.posts = resp;
    });
  }
}
