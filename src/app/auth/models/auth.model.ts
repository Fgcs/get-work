import { UserModel } from './user.model';

export class AuthModel {
  _id?: string;
  dateLogin?: Date;
  tokenType?: string;
  token?: string;
  expiresIn?: string;
  user?: UserModel;
}
