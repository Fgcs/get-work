import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpSubscriberComponent } from './sign-up-subscriber.component';

describe('SignUpSubscriberComponent', () => {
  let component: SignUpSubscriberComponent;
  let fixture: ComponentFixture<SignUpSubscriberComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignUpSubscriberComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpSubscriberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
