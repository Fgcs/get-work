import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagerCreateJobComponent } from './manager-create-job.component';

describe('ManagerCreateJobComponent', () => {
  let component: ManagerCreateJobComponent;
  let fixture: ComponentFixture<ManagerCreateJobComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ManagerCreateJobComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagerCreateJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
