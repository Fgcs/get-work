import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { PostModel } from '../models/post.model';
import { CreatePostModel } from '../models/createPost.model';
@Injectable({
  providedIn: 'root',
})
export class ManagerService {
  constructor(private http: HttpClient) {}
  getByAuthor(author: string): Observable<PostModel[]> {
    return this.http.get<PostModel[]>(
      `http://localhost:3000/posts/author/${author}`
    );
  }
  create(post: CreatePostModel): Observable<PostModel> {
    return this.http.post<PostModel>(`http://localhost:3000/posts`, post);
  }
  delete(id: string): Observable<PostModel> {
    return this.http.delete<PostModel>(`http://localhost:3000/posts/${id}`);
  }
}
