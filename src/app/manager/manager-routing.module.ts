import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ManagerCreateJobComponent } from './components/manager-create-job/manager-create-job.component';
import { ManagerHomeComponent } from  './components/manager-home/manager-home.component';
import { ManagerJobComponent } from './components/manager-job/manager-job.component';
import { ManagerOfferComponent } from './components/manager-offer/manager-offer.component';

const routes: Routes = [
  {
    path: 'manager-home',
    component: ManagerHomeComponent,
  },
  {
    path: 'manager-offer',
    component: ManagerOfferComponent,
  },
  {
    path: 'manager-job',
    component: ManagerJobComponent,
  },
  {
    path: 'manager-create-job',
    component: ManagerCreateJobComponent,
  },
  {
    path: '**',
    redirectTo: 'manager-home',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class managerRoutingModule {}
