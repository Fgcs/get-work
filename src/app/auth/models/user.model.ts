export class UserModel {
  _id?: string;
  name?: string;
  lastname?: string;
  dni?: string;
  email?: string;
  password?: string;
  accountType?: string;
  rol?: string;
  address?: string;
  status?: string;
  webSite?: string;
  createdAt?: Date;
  updatedAt?: Date;
}
