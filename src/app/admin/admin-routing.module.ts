import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminAsisstanceComponent } from './components/admin-asisstance/admin-asisstance.component';
import { AdminContentsComponent } from './components/admin-contents/admin-contents.component';
import { AdminHomeComponent } from './components/admin-home/admin-home.component';
import { AdminJobComponent } from './components/admin-job/admin-job.component';
import { AdminNavbarComponent } from './components/admin-navbar/admin-navbar.component';
import { AdminThemeComponent } from './components/admin-theme/admin-theme.component';
import { AdminTopicComponent } from './components/admin-topic/admin-topic.component';

const routes: Routes = [
  {
    path: 'admin-home',
    component: AdminHomeComponent,
  },
  {
    path: 'admin-theme',
    component: AdminThemeComponent,
  },
  {
    path: 'admin-job',
    component: AdminJobComponent,
  },
  {
    path: 'admin-asisstance',
    component: AdminAsisstanceComponent,
  },
  {
    path: 'admin-topic',
    component: AdminTopicComponent,
  },
  {
    path: 'admin-contents',
    component: AdminContentsComponent,
  },
  {
    path: 'admin-navbar',
    component: AdminNavbarComponent,
  },
  {
    path: '**',
    redirectTo: 'admin-home',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class adminRoutingModule {}
