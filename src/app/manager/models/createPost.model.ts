export class CreatePostModel {
  title!: string;
  description!: string;
  position!: string;
  company!: string;
  typeOfWorkplace!: string;
  teme!: string;
  typeJob!: string;
  address?: string;
  userId!: string;
}
